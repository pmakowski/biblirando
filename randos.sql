BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "routes" (
 "ID" INTEGER NOT NULL UNIQUE,
 "Nom" TEXT NOT NULL,
 "gpx" TEXT,
 "lat" NUMERIC,
 "lon" NUMERIC,
 "distance" NUMERIC,
 "positif" NUMERIC,
 "negatif" NUMERIC,
 "cotation" TEXT,
 "notes" TEXT,
 PRIMARY KEY("ID" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "tdm" (
 "ID" INTEGER NOT NULL,
 "rang" INTEGER NOT NULL,
 "etape" TEXT,
 "utm" TEXT,
 "azimut" TEXT,
 "positif" INTEGER,
 "negatif" INTEGER,
 "distance" INTEGER,
 "cumul_distance" INTEGER,
 "temps_segment" TEXT,
 "temps_cumul" TEXT,
 "pause" TEXT,
 "note" TEXT,
 CONSTRAINT "pk_tdm" PRIMARY KEY("ID","rang"),
 FOREIGN KEY("ID") REFERENCES "routes"("ID") ON DELETE CASCADE
);
COMMIT;
