|License|

**********
BibliRando
**********

Gestion d'une bibliothèque de randonnées.

Installation
============

Les versions sont disponibles sur la page : https://gitlab.com/pmakowski/biblirando/-/releases

Si vous avez Python (version 3.9 minimum) sur votre machine, vous pouvez l'installer en utilisant le fichier whl disponible sur la page https://gitlab.com/pmakowski/biblirando/-/releases.

On tout simplement télécharger le fichier biblirando.py.

Sous Windows, si vous n'avez pas Python, téléchargez l'exécutable disponible sur la page https://gitlab.com/pmakowski/biblirando/-/releases

Utilisation
===========

BibliRando fonctionne avec une base de données SQLite nommée randos.sqlite.

Au démarrage, si le logiciel ne trouve pas le fichier randos.sqlite dans le répertire courant ou dans votre dossier utilisateur, il vous demandera de choisir un emplacement ou une base existante.

Vous pouvez télécharger et utiliser la base de démonstration qui contient deux randonnées.

Dans le logiciel, 

- Un double click sur une ligne des randonnées amène la carte sur la randonnée.
- Filtrer permet de n'affichier dans la grille des randonnées que celles qui sont visibles sur la carte.
- Supprimer filtre affiche toutes les randonnées dans la grille.
- Ajouter une route permet d'ajouter une randonnée dans la base. Il faut fournir un fichier gpx avec un itinéraire (balise <rte>) avec des points de passage (balise <wpt>). `GPX (format de fichier) <https://fr.wikipedia.org/wiki/GPX_(format_de_fichier)>`_
- Export gpx permet de récupérer l'itinéraire.
- Export tdm permet de récupérer au format csv (lisible par un tableur) le tableau de marche.
- Export carte permet de récupérer l'image de la carte à l'écran au format png.

Pour modifier une valeur d'un champ, il faut passer en mode édition (touche F2).


Dépendances
===========

Les bibliothèques suivantes sont utilisées :

- `gpxpy <https://pypi.org/project/gpxpy/>`_
- `utm <https://pypi.org/project/utm/>`_
- `pyqtgraph <https://pypi.org/project/pyqtgraph/>`_
- `PyQt5 <https://pypi.org/project/PyQt5/>`_
- `pyqtlet2 <https://pypi.org/project/pyqtlet2/>`_
- `SQLite <https://sqlite.org/>`_
- `Leaflet <https://leafletjs.com/>`_


Les cartes sont celles d' `OpenTopoMap <https://opentopomap.org>`_


Auteur
======

BibliRando a été créé par `Philippe Makowski <https://gitlab.com/pmakowski>`_


License
=======

GNU General Public License v3.0 or later

Voir `COPYING <COPYING>`_ pour lire le texte en entier.

.. |License| image:: https://img.shields.io/badge/license-GPL%20v3.0-brightgreen.svg
   :target: COPYING
   :alt: Repository License

