#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright: (c) 2022, Philippe Makowski <pmakowski@espelida.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

""" BibliRando gestion des routes. """

__version__ = "0.0.1"

import csv
import datetime
import math
import os.path
import sys
import gpxpy
import utm
import pyqtgraph as pg
from PyQt5.QtSql import QSqlDatabase, QSqlTableModel, QSqlQuery
from PyQt5.QtWidgets import (
    QApplication,
    QVBoxLayout,
    QWidget,
    QSplitter,
    QTableView,
    QAbstractItemView,
    QStyledItemDelegate,
    QPushButton,
    QHBoxLayout,
    QFileDialog,
    QPlainTextEdit,
    QDataWidgetMapper,
    QMessageBox,
    QInputDialog,
    QSizePolicy,
)
from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtGui import QPixmap
from pyqtlet2 import L, MapWidget


def get_haversine(lat1, lng1, lat2, lng2):
    """formule de haversine en mètres"""
    _AVG_EARTH_RADIUS_M = 6371008.8
    lat1 = math.radians(lat1)
    lng1 = math.radians(lng1)
    lat2 = math.radians(lat2)
    lng2 = math.radians(lng2)
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = math.sin(lat * 0.5) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(lng * 0.5) ** 2
    return 2 * _AVG_EARTH_RADIUS_M * math.asin(math.sqrt(d))


def get_azimuth(lat1, lon1, lat2, lon2):
    """Calcul azimuth"""
    dlon = math.radians(lon2 - lon1)
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    x = math.cos(lat2) * math.sin(dlon)
    y = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(dlon)
    azimut = round(((math.atan2(x, y) * 180 / math.pi) + 360) % 360)
    return azimut


def get_elevation_distance(gpx_xml):
    gpx = gpxpy.parse(gpx_xml)
    ele_list = []
    distance_list = []
    total_distance = 0
    first = True
    for route in gpx.routes:
        for point in route.points:
            if first:
                first = False
                distance = 0
                ele_list.append(int(point.elevation))
                total_distance = 0 + distance
                distance_list.append(total_distance)
                lat_ref = float(point.latitude)
                lon_ref = float(point.longitude)
            else:
                lat = float(point.latitude)
                lon = float(point.longitude)
                partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                ele_list.append(int(point.elevation))
                total_distance = total_distance + partiel
                distance_list.append(total_distance)
                lat_ref = lat
                lon_ref = lon
    return distance_list, ele_list


def get_distance_denivele(gpx):
    total_distance = 0
    positif = 0
    negatif = 0
    first = True
    for route in gpx.routes:
        for point in route.points:
            if first:
                first = False
                distance = 0
                total_distance = 0 + distance
                lat_ref = float(point.latitude)
                lon_ref = float(point.longitude)
                ele_ref = float(point.elevation)
            else:
                lat = float(point.latitude)
                lon = float(point.longitude)
                ele = float(point.elevation)
                if (ele - ele_ref) > 0:
                    positif = positif + (ele - ele_ref)
                else:
                    negatif = negatif + (ele_ref - ele)
                partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                total_distance = total_distance + partiel
                lat_ref = lat
                lon_ref = lon
                ele_ref = ele
    return total_distance, positif, negatif


class AlignDelegate(QStyledItemDelegate):
    def displayText(self, value, locale):
        try:
            number = float(value)
        except ValueError:
            return super(AlignDelegate, self).displayText(value, locale)
        else:
            return f"{number:,.{0}f}".replace(",", " ")

    def initStyleOption(self, option, index):
        super(AlignDelegate, self).initStyleOption(option, index)
        option.displayAlignment = Qt.AlignRight | Qt.AlignVCenter


class MapWindow(QWidget):
    def __init__(self):
        # Setting up the widgets and layout
        super().__init__()

        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        databasefilename = "randos.sqlite"
        if os.path.exists(databasefilename):
            databasefile = databasefilename
        elif os.path.exists(os.path.join(os.path.expanduser("~"), databasefilename)):
            databasefile = os.path.join(os.path.expanduser("~"), databasefilename)
        else:
            db_dialog = QFileDialog(self)
            db_dialog.setFileMode(QFileDialog.AnyFile)
            db_dialog.setNameFilter("Sqlite Files (*.sqlite)")
            db_dialog.setOptions(options)
            db_dialog.setWindowTitle("Base randos.sqlite")
            db_dialog.setDirectory(os.path.expanduser("~"))
            db_dialog.selectFile(databasefilename)
            if db_dialog.exec():
                databasefile = db_dialog.selectedFiles()[0]
            else:
                exit()
        self.db = QSqlDatabase.addDatabase("QSQLITE")
        self.db.setDatabaseName(databasefile)
        self.db.open()
        self.db_tables()

        self.layout = QVBoxLayout()

        self.splitter1 = QSplitter()
        self.splitter1.setOrientation(Qt.Vertical)

        self.map_and_buttons = QWidget(self.splitter1)
        self.map_and_buttons_layout = QVBoxLayout(self.map_and_buttons)

        self.mapWidget = MapWidget()
        self.map_and_buttons_layout.addWidget(self.mapWidget)
        self.widget_buttons = QWidget()
        self.map_and_buttons_layout.addWidget(self.widget_buttons)
        self.mapWidget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.horizontalLayout = QHBoxLayout(self.widget_buttons)

        self.filter_button = QPushButton(self.widget_buttons)
        self.filter_button.setText("&Filtrer")
        self.filter_button.clicked.connect(self.filter_button_clicked)
        self.horizontalLayout.addWidget(self.filter_button)

        self.stop_filter_button = QPushButton(self.widget_buttons)
        self.stop_filter_button.setText("&Supprimer filtre")
        self.stop_filter_button.clicked.connect(self.stop_filter_button_clicked)
        self.horizontalLayout.addWidget(self.stop_filter_button)

        self.ajout_route_button = QPushButton(self.widget_buttons)
        self.ajout_route_button.setText("&Ajouter une route")
        self.ajout_route_button.clicked.connect(self.ajout_route_button_clicked)
        self.horizontalLayout.addWidget(self.ajout_route_button)

        self.supp_route_button = QPushButton(self.widget_buttons)
        self.supp_route_button.setText("Supprimer une &route")
        self.supp_route_button.clicked.connect(self.supp_route_button_clicked)
        self.horizontalLayout.addWidget(self.supp_route_button)

        self.export_gpx_button = QPushButton(self.widget_buttons)
        self.export_gpx_button.setText("Export &gpx")
        self.export_gpx_button.clicked.connect(self.export_gpx_button_clicked)
        self.horizontalLayout.addWidget(self.export_gpx_button)

        self.export_tdm_button = QPushButton(self.widget_buttons)
        self.export_tdm_button.setText("Export &tdm")
        self.export_tdm_button.clicked.connect(self.export_tdm_button_clicked)
        self.horizontalLayout.addWidget(self.export_tdm_button)

        self.export_map_button = QPushButton(self.widget_buttons)
        self.export_map_button.setText("Export &carte")
        self.export_map_button.clicked.connect(self.export_map_button_clicked)
        self.horizontalLayout.addWidget(self.export_map_button)

        self.about_button = QPushButton(self.widget_buttons)
        self.about_button.setText("À &propos")
        self.about_button.clicked.connect(self.about_button_clicked)
        self.horizontalLayout.addWidget(self.about_button)

        self.splitter2 = QSplitter()
        self.splitter2.setOrientation(Qt.Horizontal)

        # pg.setConfigOption('background', 'w')
        # pg.setConfigOption('foreground', 'k')
        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setTitle("Profil")
        self.graphWidget.setLabel("left", "Altitude (m)")
        self.graphWidget.setLabel("bottom", "Distance (m)")

        self.routes_model = QSqlTableModel(db=self.db)
        self.routes_model.setTable("routes")
        #        self.routes_model.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.routes_model.setHeaderData(0, Qt.Horizontal, "ID")
        self.routes_model.setHeaderData(1, Qt.Horizontal, "Nom")
        self.routes_model.setHeaderData(2, Qt.Horizontal, "gpx")
        self.routes_model.setHeaderData(3, Qt.Horizontal, "lat")
        self.routes_model.setHeaderData(4, Qt.Horizontal, "lon")
        self.routes_model.setHeaderData(5, Qt.Horizontal, "Distance")
        self.routes_model.setHeaderData(6, Qt.Horizontal, "Positif")
        self.routes_model.setHeaderData(7, Qt.Horizontal, "Négatif")
        self.routes_model.setHeaderData(8, Qt.Horizontal, "Cotation")
        self.routes_model.setHeaderData(9, Qt.Horizontal, "Notes")
        # self.routes_model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        # Set up the view
        self.routes_view = QTableView()
        self.routes_view.setModel(self.routes_model)
        self.routes_view.setColumnWidth(1, 135)
        self.routes_view.setColumnHidden(0, True)
        self.routes_view.setColumnHidden(2, True)
        self.routes_view.setColumnHidden(3, True)
        self.routes_view.setColumnHidden(4, True)
        self.routes_view.setColumnHidden(9, True)
        self.routes_view.setSortingEnabled(True)
        delegate = AlignDelegate(self.routes_view)
        self.routes_view.setItemDelegateForColumn(5, delegate)
        self.routes_view.setItemDelegateForColumn(6, delegate)
        self.routes_view.setItemDelegateForColumn(7, delegate)
        self.routes_view.setEditTriggers(QAbstractItemView.EditKeyPressed)
        self.routes_view.SelectionMode(QAbstractItemView.SingleSelection)
        self.routes_view.setSelectionBehavior(QTableView.SelectRows)
        self.routes_view.doubleClicked.connect(self.on_double_click_view)
        self.splitter2.addWidget(self.routes_view)

        self.notes_view = QPlainTextEdit()
        self.notes_view.setToolTip("Notes")
        mapper = QDataWidgetMapper(self)
        mapper.setModel(self.routes_model)
        mapper.addMapping(self.notes_view, self.routes_model.fieldIndex("notes"))
        selection_model = self.routes_view.selectionModel()
        selection_model.currentRowChanged.connect(mapper.setCurrentModelIndex)
        selection_model.currentRowChanged.connect(self.draw_profil)
        selection_model.currentRowChanged.connect(self.set_tdm_filter)

        self.tdm_model = QSqlTableModel(db=self.db)
        self.tdm_model.setTable("tdm")
        self.tdm_model.setHeaderData(0, Qt.Horizontal, "ID")
        self.tdm_model.setHeaderData(1, Qt.Horizontal, "rang")
        self.tdm_model.setHeaderData(2, Qt.Horizontal, "Étape")
        self.tdm_model.setHeaderData(3, Qt.Horizontal, "UTM")
        self.tdm_model.setHeaderData(4, Qt.Horizontal, "Azimut")
        self.tdm_model.setHeaderData(5, Qt.Horizontal, "Dénivelé positif")
        self.tdm_model.setHeaderData(6, Qt.Horizontal, "Dénivelé négatif")
        self.tdm_model.setHeaderData(7, Qt.Horizontal, "Distance")
        self.tdm_model.setHeaderData(8, Qt.Horizontal, "Cumul distance")
        self.tdm_model.setHeaderData(9, Qt.Horizontal, "Temps segment")
        self.tdm_model.setHeaderData(10, Qt.Horizontal, "Temps cumulé")
        self.tdm_model.setHeaderData(11, Qt.Horizontal, "Pause")
        self.tdm_model.setHeaderData(12, Qt.Horizontal, "Note")
        self.tdm_model.setSort(1, Qt.AscendingOrder)

        self.tdm_view = QTableView()
        self.tdm_view.setModel(self.tdm_model)
        self.tdm_view.setColumnHidden(0, True)
        self.tdm_view.setColumnHidden(1, True)
        self.tdm_view.setSortingEnabled(False)
        self.tdm_view.setEditTriggers(QAbstractItemView.EditKeyPressed)
        delegate = AlignDelegate(self.tdm_view)
        self.tdm_view.setItemDelegateForColumn(4, delegate)
        self.tdm_view.setItemDelegateForColumn(5, delegate)
        self.tdm_view.setItemDelegateForColumn(6, delegate)
        self.tdm_view.setItemDelegateForColumn(7, delegate)
        self.tdm_view.setItemDelegateForColumn(8, delegate)
        self.tdm_view.setItemDelegateForColumn(9, delegate)
        self.tdm_view.setItemDelegateForColumn(10, delegate)
        self.tdm_view.setItemDelegateForColumn(11, delegate)

        self.splitter2.addWidget(self.notes_view)

        self.splitter2.setSizes([300, 80])
        self.routes_view.setMaximumWidth(550)

        self.splitter1.addWidget(self.splitter2)
        self.splitter1.addWidget(self.graphWidget)
        self.splitter1.addWidget(self.tdm_view)

        self.layout.addWidget(self.splitter1)
        self.setLayout(self.layout)

        self.dict_traces = dict()

        # Working with the maps with pyqtlet
        #        self.map = L.map(self.mapWidget, '{ zoomControl:false }')
        self.map = L.map(self.mapWidget)
        self.map.setMaxZoom(15)
        self.map.setView([42.9, 1.5], 10)
        L.tileLayer("http://{s}.opentopomap.org/{z}/{x}/{y}.png").addTo(self.map)
        self.map.runJavaScript(
            f"var scale = L.control.scale( {{imperial: false}} ); scale.addTo({self.map.jsName});"
        )

        self.routes_model.select()
        self.routes_view.setCurrentIndex(self.routes_model.index(0, 0))
        self.tdm_model.select()

    def db_tables(self):
        query = QSqlQuery(db=self.db)
        query.exec("""CREATE TABLE IF NOT EXISTS "routes" (
                      "ID" INTEGER NOT NULL UNIQUE,
                      "Nom" TEXT NOT NULL,
                      "gpx" TEXT,
                      "lat" NUMERIC,
                      "lon" NUMERIC,
                      "distance" NUMERIC,
                      "positif" NUMERIC,
                      "negatif" NUMERIC,
                      "cotation" TEXT,
                      "notes" TEXT,
                      PRIMARY KEY("ID" AUTOINCREMENT))
                      """)
        query.exec("""CREATE TABLE IF NOT EXISTS "tdm" (
                      "ID" INTEGER NOT NULL,
                      "rang" INTEGER NOT NULL,
                      "etape" TEXT,
                      "utm" TEXT,
                      "azimut" TEXT,
                      "positif" INTEGER,
                      "negatif" INTEGER,
                      "distance" INTEGER,
                      "cumul_distance" INTEGER,
                      "temps_segment" TEXT,
                      "temps_cumul" TEXT,
                      "pause" TEXT,
                      "note" TEXT,
                      CONSTRAINT "pk_tdm" PRIMARY KEY("ID","rang"),
                      FOREIGN KEY("ID") REFERENCES "routes"("ID") ON DELETE CASCADE)
                   """)
        self.db.commit()

    def set_tdm_filter(self, ind):
        relation = self.routes_model.data(self.routes_model.index(ind.row(), 0, QModelIndex()))
        self.tdm_model.setFilter(f"ID = {relation}")

    def draw_profil(self, ind):
        gpx = self.routes_model.data(self.routes_model.index(ind.row(), 2, QModelIndex()))
        titre = self.routes_model.data(self.routes_model.index(ind.row(), 1, QModelIndex()))
        try:
            distance, ele = get_elevation_distance(gpx)
        except gpxpy.gpx.GPXXMLSyntaxException:
            distance = []
            ele = []
        self.graphWidget.clear()
        self.graphWidget.setTitle(f"Profil {titre}")
        self.graphWidget.setLabel("left", "Altitude (m)")
        self.graphWidget.setLabel("bottom", "Distance (m)")
        self.graphWidget.plot(distance, ele, fillLevel=-0.3, brush=(50, 50, 200, 100))
        self.graphWidget.showGrid(x=True, y=True)

    def on_double_click_view(self):
        index = self.routes_view.selectionModel().currentIndex()
        self.map.flyTo([index.sibling(index.row(), 3).data(), index.sibling(index.row(), 4).data()])

    def add_trace(self, compteur, gpx):
        matrace = L.layerGroup()
        self.add_depart(gpx, matrace)
        self.add_line(gpx, matrace)
        self.add_waypoints(gpx, matrace)
        self.dict_traces[compteur] = matrace

    def add_line(self, gpx, layer_group):
        line = []
        for route in gpx.routes:
            for point in route.points:
                line.append([point.latitude, point.longitude])
        L.polyline(line, "{opacity: 0.5, color: 'red'}").addTo(layer_group).layerName

    def add_depart(self, gpx, layer_group):
        point = gpx.routes[0].points[0]
        depart = L.marker([point.latitude, point.longitude])
        depart.bindPopup(gpx.routes[0].name)
        layer_group.addLayer(depart)

    def add_waypoints(self, gpx, layer_group):
        for waypoint in gpx.waypoints:
            map_waypoint = L.circleMarker(
                [waypoint.latitude, waypoint.longitude], "{radius: 2, opacity: 0.5, color: 'red'}"
            )
            utm_val = utm.from_latlon(float(waypoint.latitude), float(waypoint.longitude))
            map_waypoint.bindPopup(
                f"{waypoint.name}<br>{utm_val[2]}{utm_val[3]} {int(utm_val[0]):07d} {int(utm_val[1]):07d}"
            )
            layer_group.addLayer(map_waypoint)

    def filter_on_map_bound(self, event):
        lat1 = event["_northEast"]["lat"]
        lat0 = event["_southWest"]["lat"]
        lon1 = event["_northEast"]["lng"]
        lon0 = event["_southWest"]["lng"]
        str_filter = f"lat BETWEEN {lat0} and {lat1} AND lon BETWEEN {lon0} and {lon1}"
        self.routes_model.setFilter(str_filter)
        self.routes_view.setCurrentIndex(self.routes_model.index(0, 0))

    def filter_button_clicked(self):
        self.map.getBounds(self.filter_on_map_bound)

    def stop_filter_button_clicked(self):
        self.routes_model.setFilter("")
        self.routes_view.setCurrentIndex(self.routes_model.index(0, 0))

    def export_map_button_clicked(self):
        self.map.runJavaScript(f"{self.map.jsName}.zoomControl.remove();")
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        mapfilepath, okpressed = QFileDialog.getSaveFileName(
            self,
            "Sauvegarder sous",
            "",
            "Images (*.png);;All Files (*)",
            options=options,
        )
        if okpressed:
            size = self.mapWidget.contentsRect()
            img = QPixmap(size.width(), size.height())
            self.mapWidget.render(img)
            img.save(mapfilepath)
        self.map.runJavaScript(f"{self.map.jsName}.zoomControl.addTo({self.map.jsName});")

    def export_gpx_button_clicked(self, ind):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        gpxfilepath, okpressed = QFileDialog.getSaveFileName(
            self,
            "Sauvegarder sous",
            "",
            "Routes (*.gpx);;All Files (*)",
            options=options,
        )
        if okpressed:
            index = self.routes_view.selectionModel().currentIndex()
            gpx = index.sibling(index.row(), 2).data()
            with open(gpxfilepath, "w") as file:
                file.write(gpx)

    def export_tdm_button_clicked(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        tdmfilepath, okpressed = QFileDialog.getSaveFileName(
            self,
            "Sauvegarder sous",
            "",
            "Fichier csv (*.csv);;All Files (*)",
            options=options,
        )
        if okpressed:
            with open(tdmfilepath, "w") as stream:
                writer = csv.writer(
                    stream,
                    encoding="utf-8",
                    lineterminator="\n",
                    delimiter=";",
                    quoting=csv.QUOTE_NONNUMERIC,
                )
                headers = [
                    "rang",
                    "Étape",
                    "UTM",
                    "Azimut",
                    "Dénivelé positif",
                    "Dénivelé négatif",
                    "Distance",
                    "Cumul distance",
                    "Temps segment",
                    "Temps cumulé",
                    "Pause",
                    "Note",
                ]
                writer.writerow(headers)
                self.tdm_view.selectAll()
                for i in self.tdm_view.selectionModel().selectedRows():
                    row = i.row()
                    fields = []
                    for col in range(1, self.tdm_view.model().columnCount()):
                        field = self.tdm_model.record(row).value(
                            self.tdm_model.record(row).fieldName(col)
                        )
                        fields.append(field)
                    writer.writerow(fields)
                self.tdm_view.clearSelection()

    def ajout_route_button_clicked(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        gpxfilepath, _ = QFileDialog.getOpenFileName(
            self,
            "Choisir une route",
            "",
            "Routes (*.gpx);;All Files (*)",
            options=options,
        )

        if gpxfilepath:
            try:
                gpx_xml = open(gpxfilepath, "r").read()
                gpx = gpxpy.parse(gpx_xml)
                distance, positif, negatif = get_distance_denivele(gpx)
                for route in gpx.routes:
                    nom = route.name
                    lat = route.points[0].latitude
                    lon = route.points[0].longitude

                r = self.routes_model.record()
                r.setValue("Nom", nom)
                r.setValue("gpx", gpx_xml)
                r.setValue("lat", lat)
                r.setValue("lon", lon)
                r.setValue("distance", distance)
                r.setValue("positif", positif)
                r.setValue("negatif", negatif)
                self.routes_model.insertRecord(-1, r)
                self.routes_model.submitAll()
                self.routes_model.select()
                query = QSqlQuery(db=self.db)
                query.exec("SELECT max(ID) FROM routes")
                while query.next():
                    new_id = query.value(0)
                query.exec(f"SELECT gpx FROM routes where ID={new_id}")
                while query.next():
                    gpx = gpxpy.parse(query.value(0))
                    self.add_trace(new_id, gpx)
                self.map.addLayer(self.dict_traces[new_id])

                plat = 5000
                asc = 400
                desc = 500
                pause = 30
                plat, _ = QInputDialog.getInt(
                    self, "Vitesse", "À plat en m/h:", plat, 3000, 6000, 100
                )
                asc, _ = QInputDialog.getInt(
                    self, "Vitesse", "En montée en m/h:", asc, 300, 1000, 50
                )
                desc, _ = QInputDialog.getInt(
                    self, "Vitesse", "En descente en m/h:", desc, 300, 1000, 50
                )
                pause, _ = QInputDialog.getInt(
                    self, "Pause", "Temps de pause en %:", pause, 10, 40, 5
                )
                km_effort = QMessageBox.question(
                    self.parent(),
                    "Kilomètre effort",
                    "Utiliser le kilomètre effort ?",
                    QMessageBox.Yes,
                    QMessageBox.No,
                )
                km_effort = km_effort == QMessageBox.Yes
                self.remplir_tdm(new_id, gpx, plat, asc, desc, pause, km_effort)

            except (gpxpy.gpx.GPXXMLSyntaxException, UnboundLocalError, IndexError):
                QMessageBox.critical(
                    self.parent(),
                    "Erreur",
                    f"Erreur dans le traitement du fichier {gpxfilepath}",
                    QMessageBox.Ok,
                )

    def remplir_tdm(self, new_id, gpx, plat, asc, desc, pause, km_effort):
        first = True
        tdm_dict = dict()
        tdm_list = []
        for route in gpx.routes:
            for point in route.points:
                if first:
                    first = False
                    utm_val = utm.from_latlon(point.latitude, point.longitude)
                    distance = 0
                    cumul_distance = 0
                    positif = 0
                    negatif = 0
                    lat_ref = float(point.latitude)
                    lon_ref = float(point.longitude)
                    ele_ref = int(point.elevation)
                    azimut = None
                    temps_segment = 0
                    cumul_temps = 0
                    tdm_dict["Étape"] = "Départ"
                    tdm_dict[
                        "UTM"
                    ] = f"{utm_val[2]}{utm_val[3]} {int(utm_val[0]):07d} {int(utm_val[1]):07d}"
                    tdm_dict["Azimut"] = ""
                    tdm_dict["Altitude"] = ele_ref
                    tdm_dict["Dénivelé positif"] = positif
                    tdm_dict["Dénivelé négatif"] = negatif
                    tdm_dict["Distance"] = 0
                    tdm_dict["Cumul distance"] = 0
                    tdm_dict["Temps segment"] = ""
                    tdm_dict["Temps cumulé"] = ""
                    tdm_dict["Pause"] = ""
                    tdm_list.append(tdm_dict)
                else:
                    lat = float(point.latitude)
                    lon = float(point.longitude)
                    partiel = get_haversine(lat_ref, lon_ref, lat, lon)
                    distance = distance + partiel
                    if not azimut and lat != lat_ref and lon != lon_ref:
                        azimut = round(get_azimuth(lat_ref, lon_ref, lat, lon))
                    ele = point.elevation
                    if (ele - ele_ref) > 0:
                        positif = positif + (ele - ele_ref)
                    else:
                        negatif = negatif + (ele_ref - ele)
                    try:
                        pente = ((ele - ele_ref) / partiel) * 100
                    except ZeroDivisionError:
                        pente = 0
                    if pente > 10:
                        temps = 60 / asc * (ele - ele_ref)
                    elif pente < -10:
                        temps = 60 / desc * (ele_ref - ele)
                    else:
                        temps = 60 / plat * partiel
                    if km_effort:
                        temps = (
                            60 / plat * (partiel / 1000 + (positif / 125) + (negatif / 400) * 1000)
                        )
                    temps_segment = temps_segment + temps
                    tdm_dict = dict()
                    for waypoint in gpx.waypoints:
                        if (
                            waypoint.latitude == point.latitude
                            and waypoint.longitude == point.longitude
                            and waypoint.elevation == point.elevation
                            and not (waypoint.latitude == ele_ref
                                     and waypoint.longitude == lon_ref
                                     and waypoint.latitude == lat_ref)
                        ):
                            tdm_dict["Étape"] = waypoint.name
                        elif point == route.points[-1]:
                            tdm_dict["Étape"] = "Arrivée"
                    lat_ref = lat
                    lon_ref = lon
                    ele_ref = ele
                    if tdm_dict:
                        utm_val = utm.from_latlon(lat, lon)
                        tdm_dict[
                            "UTM"
                        ] = f"{utm_val[2]}{utm_val[3]} {int(utm_val[0]):07d} {int(utm_val[1]):07d}"
                        tdm_dict["Azimut"] = azimut
                        tdm_dict["Altitude"] = ele
                        tdm_dict["Dénivelé positif"] = positif
                        tdm_dict["Dénivelé négatif"] = negatif
                        tdm_dict["Distance"] = round(distance)
                        cumul_distance = cumul_distance + distance
                        tdm_dict["Cumul distance"] = round(cumul_distance)
                        tdm_dict["Temps segment"] = str(
                            datetime.timedelta(minutes=round(temps_segment))
                        )[:-3]
                        cumul_temps = cumul_temps + temps_segment
                        tdm_dict["Temps cumulé"] = str(
                            datetime.timedelta(minutes=round(cumul_temps))
                        )[:-3]
                        tdm_dict["Pause"] = str(
                            datetime.timedelta(minutes=round(temps_segment * pause / 100))
                        )[:-3]
                        if tdm_dict["Azimut"]:
                            tdm_list.append(tdm_dict)
                        distance = 0
                        temps_segment = 0
                        positif = 0
                        negatif = 0
                        azimut = None

        query = QSqlQuery(db=self.db)
        query.prepare(
            "INSERT INTO tdm (ID, rang, etape, utm, azimut, positif, negatif, distance"
            " ,cumul_distance, temps_segment, temps_cumul, pause) "
            " VALUES (:ID, :rang, :etape, :utm, :azimut, :positif, :negatif, :distance"
            " ,:cumul_distance, :temps_segment, :temps_cumul, :pause)"
        )
        for i, etape in enumerate(tdm_list):
            query.bindValue(":ID", new_id)
            query.bindValue(":rang", i)
            query.bindValue(":etape", etape["Étape"])
            query.bindValue(":utm", etape["UTM"])
            query.bindValue(":azimut", etape["Azimut"])
            query.bindValue(":positif", etape["Dénivelé positif"])
            query.bindValue(":negatif", etape["Dénivelé négatif"])
            query.bindValue(":distance", etape["Distance"])
            query.bindValue(":cumul_distance", etape["Cumul distance"])
            query.bindValue(":temps_segment", etape["Temps segment"])
            query.bindValue(":temps_cumul", etape["Temps cumulé"])
            query.bindValue(":pause", etape["Pause"])
            query.exec_()
        self.db.commit()
        self.tdm_model.submitAll()
        self.tdm_model.select()
        self.routes_view.setCurrentIndex(self.routes_model.index(0, 0))

    def supp_route_button_clicked(self):
        index = self.routes_view.selectionModel().currentIndex()
        if index.sibling(index.row(), 1).data():
            deleteconfirmation = QMessageBox.question(
                self.parent(),
                "Suppression route",
                f"Supprimer la route {index.sibling(index.row(), 1).data()}?",
                QMessageBox.Yes,
                QMessageBox.No,
            )
            if deleteconfirmation == QMessageBox.Yes:
                record_id = index.sibling(index.row(), 0).data()
                # On supprime les lignes tdm car sqlite ne le fait pas lui même
                query = QSqlQuery(db=self.db)
                query.prepare("DELETE FROM tdm WHERE ID= :ID")
                query.bindValue(":ID", record_id)
                query.exec_()
                self.db.commit()
                self.dict_traces[record_id].clearLayers()
                self.routes_model.removeRow(index.row())
                self.routes_model.submitAll()
                self.routes_model.select()
                self.routes_view.setCurrentIndex(self.routes_model.index(0, 0))
        else:
            QMessageBox.critical(
                self.parent(),
                "Erreur",
                "Sélectionner d'abord une route dans le tableau",
                QMessageBox.Ok,
            )

    def about_button_clicked(self):
        QMessageBox.information(
            self.parent(),
            "À propos de",
            f"""License GNU GPLv3
                \nCopyright (c) 2021, Philippe Makowski
                \nversion {__version__}
                \nUtilise les bibliothèques SQLite, LeafLet, utm, gpxpy, pyqtgraph, pyqtlet2 et PyQt5,
                \net une base de données SQLite""",
            QMessageBox.Ok,
        )


def main():
    app = QApplication(sys.argv)
    widget = MapWindow()
    for i in range(widget.routes_model.rowCount()):
        gpx = gpxpy.parse(widget.routes_model.record(i).value("gpx"))
        widget.add_trace(widget.routes_model.record(i).value("ID"), gpx)
    for value in widget.dict_traces.values():
        widget.map.addLayer(value)
    widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
